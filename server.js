import 'babel-polyfill';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import routes from './routes.js';

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin,X-Requested-With, Content-Type, Accept, client, enctype, token');
  next();
});

app.use(function (req, res, next) {
   req.locals = {
     secret: "topSecret"
   };
   next();
});

app.use('/api', routes);

let port = process.env.PORT || 3030;
app.listen(port, () => {
  console.log(`listening on port ${port}`);
})
