'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let promise = {
  promiseLib: _bluebird2.default
};

const pgPromise = require('pg-promise')(promise);
const connString = {
  host: 'localhost',
  port: 5432,
  database: 'auth_app',
  user: 'postgres',
  password: 'postgres',
  ssl: false
};
let db = pgPromise(connString);

exports.default = db;