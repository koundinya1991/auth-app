'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _user = require('./api/user.js');

var _country = require('./api/country.js');

var _media = require('./api/media.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let routes = _express2.default.Router();

let upload = (0, _multer2.default)().single('file');


routes.post('/login', async (req, res) => {
  console.log('the req.body is', req.body);
  let user = await (0, _user.authUser)(req.body.username, req.body.password, req.locals.secret);
  console.log('the user is', user);
  res.send({
    user: user
  });
});

routes.post('/signup', async (req, res) => {
  console.log(req);
  (0, _user.createUser)(req.body, req.locals.secret).then(result => {
    console.log('am i the token???', result);
    res.send({
      token: result.token,
      user: result.user[0]
    });
  }).catch(error => {
    res.send({
      error: error
    });
  });
});

routes.get('/user/:username', async (req, res) => {
  let user = await (0, _user.getUserByUsername)(req.params.username);
  console.log(user);
  res.send({
    user: user
  });
});

routes.post('/logout', _user.verifyToken, async (req, res) => {
  let logout = await (0, _user.logOutUser)(req.headers.token);
  res.send({
    result: logout
  });
});

routes.get('/country', async (req, res) => {
  let result = await (0, _country.listCountries)();
  res.send({
    result: result
  });
});

routes.post('/post/image', async (req, res) => {
  console.log('the req.file', req);
  upload(req, res, error => {
    console.log(req.file);
    (0, _media.putImageInBucket)(req.body.user, req.file.originalname, req.file.buffer, async location => {
      console.log('locaaaasssssssiiiiiooooonnn', location);
      if (location) {
        let response = await (0, _media.updateImage)(req.body.user, location);
        console.log('the response is', response);
        res.send({
          result: response
        });
      }
    });
  });
});

routes.get('/get/media/:username', async (req, res) => {
  let imgs = await (0, _media.getImagePathForUser)(req.params.username);
  console.log('the imgs are', imgs);
  let jobItem = [];
  var callback = (item, job) => {
    job.url = item;
    console.log(job.url);
    // job.filename = path.basename(job.img)
    jobItem.push(job);
  };
  imgs.forEach(img => {
    (0, _media.getSignedUrlForItem)(img.img_src, img, callback);
  });
  console.log('the jobItem are', jobItem);
  res.send({
    data: jobItem
  });
});

exports.default = routes;