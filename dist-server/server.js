'use strict';

require('babel-polyfill');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _routes = require('./routes.js');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express2.default)();
app.use((0, _cors2.default)());
app.use(_bodyParser2.default.urlencoded());
app.use(_bodyParser2.default.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin,X-Requested-With, Content-Type, Accept, client, enctype, token');
  // res.setHeader('Access-Control-Allow-credential',true);
  next();
});

app.use(function (req, res, next) {
  req.locals = {
    secret: "topSecret"
  };
  next();
});

app.use('/api', _routes2.default);

let port = process.env.PORT || 3030;
app.listen(port, () => {
  console.log(`listening on port ${port}`);
});