'use strict';

var _db = require('../db.js');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function listCountries() {
  let query = `
    SELECT
      *
    FROM
      country
  `;
  return _db2.default.query(query).then(data => data).catch(error => error);
};

module.exports = {
  listCountries
};