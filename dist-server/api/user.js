'use strict';

var _db = require('../db.js');

var _db2 = _interopRequireDefault(_db);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let jwtb = require('jwt-blacklist')(_jsonwebtoken2.default);

async function createUser(payload, secret) {
  console.log('the payload for create user is', payload);
  let user = await getUserByUsername(payload.username);
  console.log('the user is', user);
  if (user.length != 0) {
    return { 'error': 'User exists' };
  } else {
    console.log(payload);
    let hash = _bcrypt2.default.hashSync(payload.password, 10);
    console.log('the hash created by bcrypt is', hash);
    let query = `
            INSERT INTO app_user(username, password, firstname, lastname, email, country)
            VALUES(($1),($2), ($3), ($4),($5),($6))
            RETURNING
              username, firstname, lastname, country, email
          `;
    return _db2.default.query(query, [payload.username, hash, payload.firstname, payload.lastname, payload.email_id, payload.country]).then(userData => {
      console.log('the returning value for the user is', userData);
      let token = jwtb.sign({ user: userData.username }, secret, {
        expiresIn: '1d' //24 hours
      });
      console.log('the token inside the promise', token);
      return { token: token, user: userData };
    }).catch(error => console.error(error));
  }
}

function getUserByUsername(username) {
  console.log('the username is', username);
  let query = `
    SELECT
      username,
      firstname,
      lastname,
      email,
      country
    FROM
      app_user
    WHERE
      username=($1)
  `;
  return _db2.default.query(query, [username]).then(data => data).catch(error => error);
}

function getUserPassowrd(username) {
  let query = `
    SELECT
      *
    FROM
      app_user
    WHERE
      username=($1)
  `;
  return _db2.default.query(query, [username]).then(data => data).catch(error => error);
}

async function authUser(username, password, secret) {
  let user = await getUserPassowrd(username);
  console.log('the user who wants to login is', user);
  if (user.length == 0) {
    return { error: 'user not found', code: 404 };
  };
  console.log('the user', user);
  let validated = await _bcrypt2.default.compare(password, user[0].password);
  if (validated) {
    var token = jwtb.sign({ user: user.username }, secret, {
      expiresIn: '1d' //24 hours
    });
    return { token: token, user: user };
  } else {
    return { 'error': 'invalid username or password' };
  }
}

function verifyToken(req, res, next) {
  let token = req.headers.token;
  if (token != undefined || jwtb.verify(token)) {
    next();
  } else {
    res.send({
      error: 'No Token Found'
    });
  }
};

function logOutUser(token) {
  let blacklist = jwtb.blacklist(token);
  return blacklist;
}

module.exports = {
  getUserByUsername,
  createUser,
  authUser,
  verifyToken,
  logOutUser
};