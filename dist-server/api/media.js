'use strict';

var _db = require('../db.js');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let util = require('util');
let Minio = require('minio');

let localHost = {
  endPoint: '127.0.0.1',
  port: 9000,
  useSSL: false,
  accessKey: 'WN5VNVHRXNCJDO98GEH6',
  secretKey: '3+qPmnnq9IOxu+ghvOF3+R1KR7m4hJrXaN88QVZg'
};

let serverhost = {
  endPoint: '159.65.145.95',
  port: 9000,
  useSSL: false,
  accessKey: 'XRL2KV901OWLY13F3DUJ',
  secretKey: 'F6w8jkL/GTJBGaPmiu/4wcfAToLZHW8iGmocVS+A'
};

let minioConfig = process.env.NODE_ENV == 'dev' ? localHost : serverhost;
let minio = new Minio.Client(minioConfig);

function putImageInBucket(user, imgName, imgBuffer, callback) {
  console.log('the user is', user);
  minio.putObject('shotstack', `${user}/${imgName}`, imgBuffer, (error, etag) => {
    let location = `${user}/${imgName}`;
    if (!error) {
      callback(location);
      return;
    }
    return error;
  });
}

function updateImage(user, location) {
  console.log('the user and location are', user, location);
  let query = `
    INSERT INTO
      auth_media
      (img_src, username)
    VALUES (($1), ($2))
  `;
  return _db2.default.query(query, [location, user]).then(data => {
    console.log('the data is', data);
  }).catch(error => console.error(error));
}

function getImagePathForUser(user) {
  let query = `
    SELECT
      img_src
    FROM
      auth_media
    WHERE
      username=($1)
  `;

  return _db2.default.query(query, [user]).then(data => data).catch(error => console.error(error));
}

function getSignedUrlForItem(path, item, callback) {
  minio.presignedUrl('GET', 'shotstack', '/' + path, function (err, presignedUrl) {
    if (err) return console.log(err);
    callback(presignedUrl, item);
  });
}

module.exports = {
  putImageInBucket,
  updateImage,
  getSignedUrlForItem,
  getImagePathForUser
};