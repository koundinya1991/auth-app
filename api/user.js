import db from '../db.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
let jwtb  = require('jwt-blacklist')(jwt);
const SALT_ROUNDS = 10;

async function _createUser(payload) {
  let query = `
    INSERT INTO
      app_user(
        username,
        password,
        firstname,
        lastname,
        email,
        country
      )
      VALUES
        (($1),($2), ($3), ($4),($5),($6))
      RETURNING
        username,
        firstname,
        lastname,
        country,
        email
  `;

  return db.query(query, [
    payload.username,
    payload.hash,
    payload.firstname,
    payload.lastname,
    payload.email,
    payload.country
    ]).then(userData => {
      let token = jwtb.sign({ user: userData.username }, payload.secret, {
        expiresIn: '1d' //24 hours
      });
      return {token: token, user: userData};
    }).catch(error => error);
}


async function createUser(payload, secret) {
    let user = await getUserByUsername(payload.username);
    if(user.length != 0) {
      throw new Error('User exists');
    } else {
      let hash = bcrypt.hashSync(payload.password, SALT_ROUNDS);
      Object.assign(payload, { hash, secret });
      return _createUser(payload);
    }
}

function getUserByUsername(username) {
  let query = `
    SELECT
      username,
      firstname,
      lastname,
      email,
      country
    FROM
      app_user
    WHERE
      username=($1)
  `;
  return db.query(query,[username]).then(data => data).catch(error => error);
}

function getUserPassowrd(username) {
  let query = `
    SELECT
      *
    FROM
      app_user
    WHERE
      username=($1)
  `;
  return db.query(query,[username]).then(data => data).catch(error => error);
}

async function authUser(username, password, secret) {
  let user = await getUserPassowrd(username);
  if (user.length == 0) {
    return { error: 'user not found', code: 404 }
  };
  let validated = await bcrypt.compare(password, user[0].password);
  if (validated) {
    var token = jwtb.sign({ user: user.username }, secret, {
      expiresIn: '1d' //24 hours
    });
    return {token:token, user: user};
  } else {
    return {'error': 'invalid username or password'};
  }
}


function verifyToken(req, res, next) {
  let token = req.headers.token;
    if (token != undefined || jwtb.verify(token)) {
      next();
    } else {
      res.send({
        error: 'No Token Found'
      });
    }
  };

  function logOutUser(token) {
    let blacklist = jwtb.blacklist(token);
    return blacklist;
  }

module.exports = {
  getUserByUsername,
  createUser,
  authUser,
  verifyToken,
  logOutUser
};
