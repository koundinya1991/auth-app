import db from '../db.js';

function listCountries() {
  let query = `
    SELECT
      *
    FROM
      country
  `;
  return db.query(query).then(data => data).catch(error => error);
};

module.exports = {
  listCountries
};
