import bluebird from 'bluebird';

let promise = {
  promiseLib: bluebird
}

const pgPromise = require('pg-promise')(promise);
const connString = {
  host: 'localhost',
  port: 5432,
  database: 'auth_app',
  user: 'postgres',
  password: 'postgres',
  ssl: false
}
let db = pgPromise(connString)


export default db;
