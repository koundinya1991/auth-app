import express from 'express';
let routes = express.Router();
import multer from 'multer';
let upload = multer().single('file');
import path from 'path';

import { getUserByUsername, createUser, authUser, logOutUser, verifyToken } from './api/user.js';
import { listCountries } from './api/country.js';
import { putImageInBucket, updateImage, getImagePathForUser, getSignedUrlForItem } from './api/media.js';


routes.post('/login', async (req, res) => {
  authUser(req.body.username, req.body.password, req.locals.secret).then(result => {
    console.log(result);
    res.send({
      token: result.token,
      user:  result.user[0],
      status: 200
    });
  }).catch(error => {
    res.send({error:error.message, status: 404});
  });

});

routes.post('/signup', async (req, res) => {
  createUser(req.body, req.locals.secret).then(result => {

    res.send({
      token: result.token,
      user:  result.user[0],
      status: 200
    });
  }).catch(error => {
    res.send({error:error.message, status: 404});
  });
});

routes.get('/user/:username', async (req, res) => {
  let user = await getUserByUsername(req.params.username);
  res.send({
    user: user
  });
});

routes.post('/logout', verifyToken, async(req, res) => {
  let logout = await logOutUser(req.headers.token);
  res.send({
    result: logout
  });
});

routes.get('/country', async(req,res) => {
  let result = await listCountries();
  res.send({
    result: result
  });
});

routes.post('/post/image', async(req, res) => {
  upload(req, res, (error) => {
    console.log('req.file.originalname', req.file.originalname);
    putImageInBucket(req.body.user, req.file.originalname, req.file.buffer, async (location) => {
      if(location) {
        let response = await updateImage(req.body.user, location);
        res.send({
          result: response
        });
      }
    });
  });
});

routes.get('/get/media/:username', async(req, res) => {
  let imgs = await getImagePathForUser(req.params.username);
  let jobItem = []
  var callback = (item, job)=> {
    job.url = item;
    jobItem.push(job)
  }
  imgs.forEach(img => {
    getSignedUrlForItem(img.img_src, img, callback);
  });
  res.send({
    data:jobItem
  })
});



export default routes;
