--
-- PostgreSQL database dump
--

-- Dumped from database version 11.0
-- Dumped by pg_dump version 11.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_user (
    id integer NOT NULL,
    username text,
    password text,
    firstname text,
    lastname text,
    email text,
    country text
);


ALTER TABLE public.app_user OWNER TO postgres;

--
-- Name: app_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_user_id_seq OWNER TO postgres;

--
-- Name: app_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_user_id_seq OWNED BY public.app_user.id;


--
-- Name: auth_media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_media (
    id integer NOT NULL,
    img_src text,
    username text
);


ALTER TABLE public.auth_media OWNER TO postgres;

--
-- Name: auth_media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_media_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_media_id_seq OWNER TO postgres;

--
-- Name: auth_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_media_id_seq OWNED BY public.auth_media.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.country (
    id integer NOT NULL,
    cow_id integer NOT NULL,
    name character varying(255) NOT NULL,
    abbreviation character varying(5) NOT NULL
);


ALTER TABLE public.country OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id_seq OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.country_id_seq OWNED BY public.country.id;


--
-- Name: media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.media (
    id integer NOT NULL,
    img_src text,
    "user" text
);


ALTER TABLE public.media OWNER TO postgres;

--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.media_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.media_id_seq OWNER TO postgres;

--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.media_id_seq OWNED BY public.media.id;


--
-- Name: app_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user ALTER COLUMN id SET DEFAULT nextval('public.app_user_id_seq'::regclass);


--
-- Name: auth_media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_media ALTER COLUMN id SET DEFAULT nextval('public.auth_media_id_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country ALTER COLUMN id SET DEFAULT nextval('public.country_id_seq'::regclass);


--
-- Name: media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media ALTER COLUMN id SET DEFAULT nextval('public.media_id_seq'::regclass);


--
-- Data for Name: app_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_user (id, username, password, firstname, lastname, email, country) FROM stdin;
\.


--
-- Data for Name: auth_media; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_media (id, img_src, username) FROM stdin;
25	test-p/sanjeevan-satheeskumar-322856-unsplash.jpg	test-p
26	test-p/sanjeevan-satheeskumar-322856-unsplash.jpg	test-p
27	test-p/sanjeevan-satheeskumar-322856-unsplash.jpg	test-p
28	test-p/aishath-naj-662589-unsplash.jpg	test-p
29	test-p/kyle-johnson-387021-unsplash.jpg	test-p
30	test-b/sanjeevan-satheeskumar-322856-unsplash.jpg	test-b
31	test-b/aishath-naj-662589-unsplash.jpg	test-b
32	test-b/sanjeevan-satheeskumar-322856-unsplash.jpg	test-b
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.country (id, cow_id, name, abbreviation) FROM stdin;
1	2	United States of America	USA
2	20	Canada	CAN
3	31	Bahamas	BHM
4	40	Cuba	CUB
5	41	Haiti	HAI
6	42	Dominican Republic	DOM
7	51	Jamaica	JAM
8	52	Trinidad and Tobago	TRI
9	53	Barbados	BAR
10	54	Dominica	DMA
11	55	Grenada	GRN
12	56	St. Lucia	SLU
13	57	St. Vincent and the Grenadines	SVG
14	58	Antigua & Barbuda	AAB
15	60	St. Kitts and Nevis	SKN
16	70	Mexico	MEX
17	80	Belize	BLZ
18	90	Guatemala	GUA
19	91	Honduras	HON
20	92	El Salvador	SAL
21	93	Nicaragua	NIC
22	94	Costa Rica	COS
23	95	Panama	PAN
24	100	Colombia	COL
25	101	Venezuela	VEN
26	110	Guyana	GUY
27	115	Suriname	SUR
28	130	Ecuador	ECU
29	135	Peru	PER
30	140	Brazil	BRA
31	145	Bolivia	BOL
32	150	Paraguay	PAR
33	155	Chile	CHL
34	160	Argentina	ARG
35	165	Uruguay	URU
36	200	United Kingdom	UKG
37	205	Ireland	IRE
38	210	Netherlands	NTH
39	211	Belgium	BEL
40	212	Luxembourg	LUX
41	220	France	FRN
42	221	Monaco	MNC
43	223	Liechtenstein	LIE
44	225	Switzerland	SWZ
45	230	Spain	SPN
46	232	Andorra	AND
47	235	Portugal	POR
48	240	Hanover	HAN
49	245	Bavaria	BAV
50	255	Germany	GMY
51	260	German Federal Republic	GFR
52	265	German Democratic Republic	GDR
53	267	Baden	BAD
54	269	Saxony	SAX
55	271	Wuerttemburg	WRT
56	273	Hesse Electoral	HSE
57	275	Hesse Grand Ducal	HSG
58	280	Mecklenburg Schwerin	MEC
59	290	Poland	POL
60	300	Austria-Hungary	AUH
61	305	Austria	AUS
62	310	Hungary	HUN
63	315	Czechoslovakia	CZE
64	316	Czech Republic	CZR
65	317	Slovakia	SLO
66	325	Italy	ITA
67	327	Papal States	PAP
68	329	Two Sicilies	SIC
69	331	San Marino	SNM
70	332	Modena	MOD
71	335	Parma	PMA
72	337	Tuscany	TUS
73	338	Malta	MLT
74	339	Albania	ALB
75	341	Montenegro	MNG
76	343	Macedonia	MAC
77	344	Croatia	CRO
78	345	Yugoslavia	YUG
79	346	Bosnia and Herzegovina	BOS
80	347	Kosovo	KOS
81	349	Slovenia	SLV
82	350	Greece	GRC
83	352	Cyprus	CYP
84	355	Bulgaria	BUL
85	359	Moldova	MLD
86	360	Romania	ROM
87	365	Russia	RUS
88	366	Estonia	EST
89	367	Latvia	LAT
90	368	Lithuania	LIT
91	369	Ukraine	UKR
92	370	Belarus	BLR
93	371	Armenia	ARM
94	372	Georgia	GRG
95	373	Azerbaijan	AZE
96	375	Finland	FIN
97	380	Sweden	SWD
98	385	Norway	NOR
99	390	Denmark	DEN
100	395	Iceland	ICE
101	402	Cape Verde	CAP
102	403	Sao Tome and Principe	STP
103	404	Guinea-Bissau	GNB
104	411	Equatorial Guinea	EQG
105	420	Gambia	GAM
106	432	Mali	MLI
107	433	Senegal	SEN
108	434	Benin	BEN
109	435	Mauritania	MAA
110	436	Niger	NIR
111	437	Ivory Coast	CDI
112	438	Guinea	GUI
113	439	Burkina Faso	BFO
114	450	Liberia	LBR
115	451	Sierra Leone	SIE
116	452	Ghana	GHA
117	461	Togo	TOG
118	471	Cameroon	CAO
119	475	Nigeria	NIG
120	481	Gabon	GAB
121	482	Central African Republic	CEN
122	483	Chad	CHA
123	484	Congo	CON
124	490	Democratic Republic of the Congo	DRC
125	500	Uganda	UGA
126	501	Kenya	KEN
127	510	Tanzania	TAZ
128	511	Zanzibar	ZAN
129	516	Burundi	BUI
130	517	Rwanda	RWA
131	520	Somalia	SOM
132	522	Djibouti	DJI
133	530	Ethiopia	ETH
134	531	Eritrea	ERI
135	540	Angola	ANG
136	541	Mozambique	MZM
137	551	Zambia	ZAM
138	552	Zimbabwe	ZIM
139	553	Malawi	MAW
140	560	South Africa	SAF
141	565	Namibia	NAM
142	570	Lesotho	LES
143	571	Botswana	BOT
144	572	Swaziland	SWA
145	580	Madagascar	MAG
146	581	Comoros	COM
147	590	Mauritius	MAS
148	591	Seychelles	SEY
149	600	Morocco	MOR
150	615	Algeria	ALG
151	616	Tunisia	TUN
152	620	Libya	LIB
153	625	Sudan	SUD
154	626	South Sudan	SSD
155	630	Iran	IRN
156	640	Turkey	TUR
157	645	Iraq	IRQ
158	651	Egypt	EGY
159	652	Syria	SYR
160	660	Lebanon	LEB
161	663	Jordan	JOR
162	666	Israel	ISR
163	670	Saudi Arabia	SAU
164	678	Yemen Arab Republic	YAR
165	679	Yemen	YEM
166	680	Yemen People's Republic	YPR
167	690	Kuwait	KUW
168	692	Bahrain	BAH
169	694	Qatar	QAT
170	696	United Arab Emirates	UAE
171	698	Oman	OMA
172	700	Afghanistan	AFG
173	701	Turkmenistan	TKM
174	702	Tajikistan	TAJ
175	703	Kyrgyzstan	KYR
176	704	Uzbekistan	UZB
177	705	Kazakhstan	KZK
178	710	China	CHN
179	712	Mongolia	MON
180	713	Taiwan	TAW
181	730	Korea	KOR
182	731	North Korea	PRK
183	732	South Korea	ROK
184	740	Japan	JPN
185	750	India	IND
186	760	Bhutan	BHU
187	770	Pakistan	PAK
188	771	Bangladesh	BNG
189	775	Myanmar	MYA
190	780	Sri Lanka	SRI
191	781	Maldives	MAD
192	790	Nepal	NEP
193	800	Thailand	THI
194	811	Cambodia	CAM
195	812	Laos	LAO
196	816	Vietnam	DRV
197	817	Republic of Vietnam	RVN
198	820	Malaysia	MAL
199	830	Singapore	SIN
200	835	Brunei	BRU
201	840	Philippines	PHI
202	850	Indonesia	INS
203	860	East Timor	ETM
204	900	Australia	AUL
205	910	Papua New Guinea	PNG
206	920	New Zealand	NEW
207	935	Vanuatu	VAN
208	940	Solomon Islands	SOL
209	946	Kiribati	KIR
210	947	Tuvalu	TUV
211	950	Fiji	FIJ
212	955	Tonga	TON
213	970	Nauru	NAU
214	983	Marshall Islands	MSI
215	986	Palau	PAL
216	987	Federated States of Micronesia	FSM
217	990	Samoa	WSM
\.


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.media (id, img_src, "user") FROM stdin;
\.


--
-- Name: app_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_user_id_seq', 40, true);


--
-- Name: auth_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_media_id_seq', 32, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.country_id_seq', 217, true);


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.media_id_seq', 1, false);


--
-- Name: app_user app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);


--
-- Name: auth_media auth_media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_media
    ADD CONSTRAINT auth_media_pkey PRIMARY KEY (id);


--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id, cow_id);


--
-- Name: media media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

